package net.foodieandroid.powem.foodieandroid.AddPost;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;


import net.foodieandroid.powem.foodieandroid.Firebase.FirebaseHelper;
import net.foodieandroid.powem.foodieandroid.Food;
import net.foodieandroid.powem.foodieandroid.GenericUtil.DateUtil;
import net.foodieandroid.powem.foodieandroid.User;

import org.json.JSONException;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

/**
 * Created by Power on 18/08/17. With lots of love and affection :) xoxo
 */

public class AddPostPresenter implements  AddPostContract.ActionListener{
    private final AddPostContract.View mView;
    private final Context mContext;
    final String[] currentUserAmountDue = {""};
    final Boolean[] hasAnOrderBeenPostedToday = {true};


    private static final String TAG = "Add Food Presenter";

    public AddPostPresenter(AddPostContract.View view, Context context){
        this.mView = view;
        this.mContext = context;
    }

    @Override
    public View.OnClickListener onClickAddFoodButton() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mView.disableAddFoodButton();
                postFoodToDb(new Callable<String>() {
                    @Override
                    public String call() throws Exception {
                        return showPostsScreen();
                    }
                });

            }
        };
    }

    @Override
    public void initView() {
        mView.showProgressDialog();
        updateAllObjects();
    }

    private void updateAllObjects() {
//        FirebaseHelper.updateAllTheDataBruuhhhhhh(new Callable<String>() {
//            @Override
//            public String call() throws Exception {
//                return updateView();
//            }
//        });
    }

    private String updateView() {
        mView.setDayText(DateUtil.getDayOfWeek());
        try {
            mView.setFoodText(FirebaseHelper.getFoodForToday(DateUtil.getDayOfWeek().toUpperCase()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            mView.setPriceText(FirebaseHelper.getPriceForToday(DateUtil.getDayOfWeek().toUpperCase()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mView.hideProgressDialog();
        return "";
    }


    public String checkCurrentAmountDueThenUpdateUserBalanceAndLastOrderPostedDate(){

        checkCurrentUserAmountDue(new Callable<String>() {
            @Override
            public String call() throws Exception {
                return updateUserAmountDueAndLastOrderPostedDate(new Callable<String>() {
                    @Override
                    public String call() throws Exception {
                        return showPostsScreen();
                    }
                });
            }
        });
        return "";
    }

    private String showPostsScreen() {
        mView.launchPostsScreen();
        return "";
    }

    private String checkIfAnOrderHasBeenPostedToday(final Callable<String> command){ //not using this anymore

        String key = FirebaseHelper.getUserKeyInCustomUserDb();
        DatabaseReference usersRef = FirebaseHelper.getUserDbRef().child(key);

        ValueEventListener userListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
//
                Date lastOrderPostedDate = user.lastOrderPostedDate;
                if (DateUtil.checkIfDateIsBeforeToday(lastOrderPostedDate)){
                    hasAnOrderBeenPostedToday[0] = false;

                    try {
                        command.call();
                    } catch (Exception e) {
                        mView.hideProgressDialog();
                        mView.showAnErrorOccuredToast();
                        e.printStackTrace();
                    }
                }else {

                    mView.hideProgressDialog();
                    mView.showOrderAlreadyPlacedTodayMessage();
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "addOrdersPresenter:onCancelled", databaseError.toException());
                mView.hideProgressDialog();
                Toast.makeText(mContext, databaseError.getMessage(),
                        Toast.LENGTH_SHORT).show();

            }
        };
        usersRef.addListenerForSingleValueEvent(userListener);

        return "";
    }

    private String postFoodToDb(final Callable<String> command) {
        DatabaseReference mDatabase = FirebaseHelper.getFoodDbRef();

        final int numberOfPeople = mView.getNumberOfOrders();
        final int[] y = {0};

            final Food order = new Food(mView.getUserId(), mView.getTimeNow(), FirebaseHelper.getUserDisplayName(), FirebaseHelper.getUserEmail(),mView.getFoodText(), mView.getRatingText(),mView.getVenueText());
            String orderId = mDatabase.push().getKey();
            // push order to 'orders' node using the orderId
            mDatabase.child(orderId).setValue(order, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    if(databaseError!=null){
                        mView.hideProgressDialog();
                        Toast.makeText(mContext, databaseError.getMessage(),
                                Toast.LENGTH_SHORT).show();
                    }
                    else{
                        try {
                            command.call();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                }
            });

        return  "";
    }



    public String checkCurrentUserAmountDue(final Callable<String> command) {

        DatabaseReference usersRef = FirebaseHelper.getUserDbRef();

        ValueEventListener userListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                Log.d("AMOUNT DUE:------ ", user.amountDue);

                currentUserAmountDue[0] = user.amountDue;
                try {
                    command.call();
                } catch (Exception e) {
                    mView.hideProgressDialog();
                    mView.showAnErrorOccuredToast();
                    e.printStackTrace();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "addOrdersPresenter:onCancelled", databaseError.toException());
                mView.hideProgressDialog();
                Toast.makeText(mContext, databaseError.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        };
        usersRef.addListenerForSingleValueEvent(userListener);

        return currentUserAmountDue[0];
    }


    private String updateUserAmountDueAndLastOrderPostedDate(final Callable<String> command) {
        int numberOfPeople = mView.getNumberOfOrders();
        Integer newAmountDue = Integer.parseInt(currentUserAmountDue[0]) + Integer.parseInt(mView.getPriceText())*numberOfPeople;

        DatabaseReference mUsersDbRef = FirebaseHelper.getUserDbRef();
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/amountDue/", newAmountDue.toString());
        childUpdates.put("/latestOrderDate/", mView.getTimeNow());

        mUsersDbRef.updateChildren(childUpdates, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if(databaseError!=null){
                    mView.hideProgressDialog();
                    Toast.makeText(mContext, databaseError.getMessage(),
                            Toast.LENGTH_SHORT).show();

                    try {
                        command.call();
                    } catch (Exception e) {
                        mView.hideProgressDialog();
                        mView.showAnErrorOccuredToast();
                        e.printStackTrace();
                    }

                }
                else{
                    mView.hideProgressDialog();
                    mView.launchPostsScreen();
                }
            }
        });

        return "justBecauseIHaveToReturnADamnString";
    }

}
