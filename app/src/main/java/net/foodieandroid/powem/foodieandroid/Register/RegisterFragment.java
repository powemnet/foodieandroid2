package net.foodieandroid.powem.foodieandroid.Register;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


import net.foodieandroid.powem.foodieandroid.Posts.PostsActivity;
import net.foodieandroid.powem.foodieandroid.R;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Power on 16/08/17. With lots of love and affection :) xoxo
 */

public class RegisterFragment extends Fragment implements RegisterContract.View{

    private RegisterContract.ActionListener presenter;
    private Button registerButton;
    private EditText emailAddress;
    private EditText password;
    private EditText userName;
    private TextView phoneError;
    private TextView passwordError;
    private ProgressBar progressBar;
    private Spinner companySpinner;

    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private ArrayList<String> companyArraySpinner;
    ArrayAdapter<String> companySpinnerAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.register_fragment, container, false);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        //initialise presenter
        presenter = new RegisterPresenter(this,mAuth, getContext());

        //initialise views
        registerButton = (Button)root.findViewById(R.id.btn_register_on_register_screen);
        userName = (EditText)root.findViewById(R.id.et_user_name);
        emailAddress = (EditText)root.findViewById(R.id.et_email_address_register);
        password = (EditText) root.findViewById(R.id.et_pin_register);
        progressBar = (ProgressBar)root.findViewById(R.id.progressBar1);


        //set onclick listeners
        registerButton.setOnClickListener(presenter.onClickRegisterButton());


        //set up text changed listeners
        userName.addTextChangedListener(presenter.onUserNameTextChanged());
        emailAddress.addTextChangedListener(presenter.onPhoneNumberTextChanged());
        password.addTextChangedListener(presenter.onPinTextChanged());

        return root;

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void showError() {
        Toast.makeText(getContext(), "Can't reach the Foodie network", Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean phoneNumberValid() {
        int length = emailAddress.getText().length();
        boolean valid = true;
        if (length<10){
            valid = false;
        }
        return valid;
    }

    @Override
    public void showPhoneError() {
        emailAddress.setError(getString(R.string.wrong_email_address));

    }

    @Override
    public boolean pinValid() {
        int length = password.getText().length();
        boolean valid = true;
        if (length<4){
            valid = false;
        }
        return valid;
    }

    @Override
    public void showPinError() {
        password.setError(getString(R.string.wrong_pin));
    }

    @Override
    public void hidePinError() {
        password.setError(null);
    }

    @Override
    public void hidePhoneError() {
        emailAddress.setError(null);
    }
    @Override
    public void showProgressDailog() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public String getUserName() {
        return userName.getText().toString();
    }

    @Override
    public void hideuserNameError() {

    }

    @Override
    public void launchPostsScreen() {
        Intent intent = new Intent(getActivity(), PostsActivity.class);
        startActivity(intent);
    }

    @Override
    public void showFailureError() {
        Toast.makeText(getContext(), "Registration Failed",
                Toast.LENGTH_LONG).show();
    }

    @Override
    public int getPasswordLength() {
        return password.getText().length();
    }

    @Override
    public Date getTimeNow() {
        return new Date();
    }


    @Override
    public String getCompanySpinnerText() {
        return companySpinner.getSelectedItem().toString();

    }

    @Override
    public void updatedSpinnerValues(List<String> mCompanies) {

        for (String company: mCompanies){
//            Log.d("Company", "LOG HERE-------------"+company.companyName);
            companyArraySpinner.add(company);

        }
        companySpinnerAdapter.notifyDataSetChanged();
        companySpinner.setAdapter(companySpinnerAdapter);

        hideProgressBar();

    }

    @Override
    public void showAnErrorOccuredError() {
        Toast.makeText(getActivity(), "An error occured", Toast.LENGTH_SHORT).show();
    }

    public String getEmailAddress() {
        return emailAddress.getText().toString();
    }

    public String getPassword() {
        return password.getText().toString();
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
//        updateUI(currentUser);
    }
}
