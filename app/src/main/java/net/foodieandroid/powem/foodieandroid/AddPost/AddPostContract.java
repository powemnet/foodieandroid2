package net.foodieandroid.powem.foodieandroid.AddPost;


import android.view.View;


import java.util.Date;

/**
 * Created by Power on 18/08/17. With lots of love and affection :) xoxo
 */

public interface AddPostContract {
    interface View{


        Date getTimeNow();

        String getUserId();

        void hideProgressDialog();

        void launchPostsScreen();

        void showProgressDialog();

        void showAnErrorOccuredToast();

        void showOrderAlreadyPlacedTodayMessage();

        int getNumberOfOrders();

        void showEnterOrdersToast();

        void setDayText(String dayOfWeek);

        void setFoodText(String foodForToday);

        void setPriceText(String priceForToday);

        String getFoodText();

        String getPriceText();

        String getRatingText();

        String getVenueText();

        void disableAddFoodButton();
    }

    interface ActionListener{


        android.view.View.OnClickListener onClickAddFoodButton();

        void initView();
    }
}
