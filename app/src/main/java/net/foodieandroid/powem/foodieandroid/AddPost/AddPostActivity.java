package net.foodieandroid.powem.foodieandroid.AddPost;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import net.foodieandroid.powem.foodieandroid.BaseActivity;
import net.foodieandroid.powem.foodieandroid.R;


/**
 * Created by Power on 18/08/17. With lots of love and affection :) xoxo
 */

public class AddPostActivity extends BaseActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        AddPostFragment fragment = new AddPostFragment();
        fragmentTransaction.add(R.id.content_frame_base_activity,fragment);
        fragmentTransaction.commit();

    }
}
