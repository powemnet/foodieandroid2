package net.foodieandroid.powem.foodieandroid;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;

/**
 * Created by power on 11/11/17.
 */

public class BaseActivity extends AppCompatActivity {
    private ActionBar actionBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_activity);

        actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(false);


    }
}
