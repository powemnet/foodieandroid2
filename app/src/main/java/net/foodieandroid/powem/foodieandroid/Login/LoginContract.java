package net.foodieandroid.powem.foodieandroid.Login;

import android.app.Activity;

/**
 * Created by power on 11/11/17.
 */

public interface LoginContract {


    interface View{

        String getEmailAddress();

        String getPassword();

        void showError();

        boolean phoneNumberValid();

        void showPhoneError();

        boolean pinValid();

        void showPinError();

        void hidePinError();

        void hidePhoneError();

        void showSpinner();

        void hideSpinner();

        Activity getActivity();

        void launchPostsScreen();

        void showUpdateDialog();

        void showEnterEmailToast();

        void showAnErrorOccuredToast();

        void showAccountDeosntExistToast();

        void showEmailSentToast();
    }

    interface ActionListener{


        android.view.View.OnClickListener onClickLogInButton();

        android.view.View.OnClickListener onClickRegisterButton();

        void launchPlayStore();

        android.view.View.OnClickListener onForgotPassword();
    }
}
