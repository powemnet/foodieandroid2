package net.foodieandroid.powem.foodieandroid.RecylerViewHelperClasses;

import android.view.View;

/**
 * Created by Power on 22/08/17. With lots of love and affection :) xoxo
 */

public  interface ClickListener{
    public void onClick(View view, int position);
    public void onLongClick(View view, int position);
}
