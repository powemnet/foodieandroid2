package net.foodieandroid.powem.foodieandroid.Posts;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;


import net.foodieandroid.powem.foodieandroid.AddPost.AddPostActivity;
import net.foodieandroid.powem.foodieandroid.Login.LoginActivity;
import net.foodieandroid.powem.foodieandroid.R;
import net.foodieandroid.powem.foodieandroid.RecylerViewHelperClasses.RecylerDividerItemDecoration;
import net.foodieandroid.powem.foodieandroid.SnackbarDismissListener;

import java.util.Date;

/**
 * Created by Power on 14/08/17. With lots of love and affection :) xoxo
 */

public class PostsActivity extends AppCompatActivity implements  PostsContract.View{

    private FloatingActionButton btnAddPost;
    private PostsContract.ActionListener presenter;
    private ProgressBar spinner;
    private SwipeRefreshLayout swipeRefreshOrders;


    //firebase
    private FirebaseAuth mAuth;
    private DatabaseReference mPostsReference;
    private ValueEventListener mOrdersListener;

    //recyler view stuff
    private RecyclerView mOrdersRecycler;
    private PostsAdapter mAdapter;


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.posts_activity);

//        //intialise firebase
        mAuth = FirebaseAuth.getInstance();
//        mPostsReference = FirebaseDatabase.getInstance().getReference()
//                .child("orders"); //// TODO: 19/08/17  change this to only return this user's orders

        //initialise presenter
        presenter = new PostsPresenter(this,mAuth, getApplicationContext());


        //initialise views
        btnAddPost = (FloatingActionButton)findViewById(R.id.floating_btn_add_order);
        mOrdersRecycler = (RecyclerView) findViewById(R.id.recycler_food);
        spinner = (ProgressBar)findViewById(R.id.progressBar_orders);
//        swipeRefreshOrders = (SwipeRefreshLayout) findViewById(R.id.swiperefreshOrders);

        //set layout manager
        mOrdersRecycler.setLayoutManager(new LinearLayoutManager(this));

        //add decorator
        mOrdersRecycler.addItemDecoration(new RecylerDividerItemDecoration(PostsActivity.this));

        //set onclick listeners
        btnAddPost.setOnClickListener(presenter.onClickAddPost());
//        swipeRefreshOrders.setOnRefreshListener(presenter.onRefreshListener());

    }

    @Override
    public void onStart() {
        super.onStart();
        mAdapter = new PostsAdapter(this,mAuth, this,presenter);
        mOrdersRecycler.setAdapter(mAdapter);
    }



    @Override
    public void onStop() {
        super.onStop();

        // Remove post value event listener
        if (mOrdersListener != null) {
            mPostsReference.removeEventListener(mOrdersListener);
        }

        // Clean up users listener
        mAdapter.cleanupListener();
    }

    @Override
    public void launchAddPostScreen() {
        Intent intent = new Intent(this, AddPostActivity.class);
        startActivity(intent);
    }

    @Override
    public void launchLoginScreen() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    @Override
    public void showSpinner() {
        spinner.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressDialog() {
        spinner.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showUserNotApprovedToast() {
        Snackbar snackbar = Snackbar.make(findViewById(R.id.order_activity_coordinator_layout),
                "Your account has not been approved. Contact your Provider with a smile :)", Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(R.string.dismiss_string, new SnackbarDismissListener());
        snackbar.show();

    }

    @Override
    public void showUserDeactivatedToast() {

        Snackbar snackbar = Snackbar.make(findViewById(R.id.order_activity_coordinator_layout),
                "Your cannot order food. You probably haven't yet paid lol! Contact Admin", Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(R.string.dismiss_string, new SnackbarDismissListener());
        snackbar.show();

    }

    @Override
    public void showItsAlready11AMToast() {

        Snackbar snackbar = Snackbar.make(findViewById(R.id.order_activity_coordinator_layout),
                "Sorry man.. It's past 11:00 AM. Wait for tomorrow", Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(R.string.dismiss_string, new SnackbarDismissListener());
        snackbar.show();
    }
    @Override
    public void refreshView() {
//        mAdapter.refresh();
    }

    @Override
    public void hideSwipeRefresh() {
        swipeRefreshOrders.setRefreshing(false);
    }

    @Override
    public void showItsAWeekEndMessage() {
        Snackbar snackbar = Snackbar.make(findViewById(R.id.order_activity_coordinator_layout),
                "It's a weekend. Go home", Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(R.string.dismiss_string, new SnackbarDismissListener());
        snackbar.show();
    }

    @Override
    public void showAnErrorOccuredToast() {
        Toast.makeText(this,"An error occurred", Toast.LENGTH_SHORT).show();
    }

    @Override
    public Context getContext() {
        return getApplicationContext();
    }

    @Override
    public Date getTimeNow() {
        return new Date();
    }

    @Override
    public void showYourDataIsStillUpdatingMessage() {
        Snackbar snackbar = Snackbar.make(findViewById(R.id.order_activity_coordinator_layout),
                "Your Data is still updating. Please turn on your internet", Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(R.string.dismiss_string, new SnackbarDismissListener());
        snackbar.show();
    }

    @Override
    public void showMenuScreen() {
//        Intent intent = new Intent(this, MenuActivity.class);
//        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.overflow, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_followers:
                presenter.onClickFollowers();
                return true;

            case R.id.action_logout:
                logUserOut();
                return true;


            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }


    private void logUserOut() {
        mAuth.signOut();
        Intent intent = new Intent (this, LoginActivity.class);
        startActivity(intent);
    }

    private void showInfoScreen() {
//        Intent intent = new Intent(PostsActivity.this, InfoActivity.class);
//        startActivity(intent);
    }
}

