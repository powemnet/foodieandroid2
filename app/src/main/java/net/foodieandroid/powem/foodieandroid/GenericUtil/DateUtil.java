package net.foodieandroid.powem.foodieandroid.GenericUtil;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Power on 25/08/17. With lots of love and affection :) xoxo
 */

public class DateUtil {
    private static Date timeYesterday;

    public static Date getTimeAtMidnightTonight(){

        DateTime today = new DateTime().withTimeAtStartOfDay();
        DateTime tomorrow = today.plusDays(1).withTimeAtStartOfDay();

        return tomorrow.toDate(); //covert joda time to java date
    }

    public static boolean checkIfDateIsBeforeToday(Date date){
        boolean rtnValue = false;

        DateTime today = new DateTime().withTimeAtStartOfDay();
        DateTime dateInComparison = new DateTime(date).withTimeAtStartOfDay();

        Date todayDate = today.toDate();
        Date dateInComparisonDate = dateInComparison.toDate();

        if(dateInComparisonDate.before(todayDate)){
            rtnValue = true;
        }

        return  rtnValue;
    }

    public static Date getTimeToday() {
        DateTime today = new DateTime().withTimeAtStartOfDay();

        return today.toDate();
    }

    public static String formartDateToShowHoursAndMinutes(Date date){

        SimpleDateFormat hourMinuteDateFormart = new SimpleDateFormat("HH:mm:ss");
        String formattedDate = hourMinuteDateFormart.format(date);

        return formattedDate;
    }

    public static String formartDateToShowDayMonthYearTime(Date date){

        SimpleDateFormat hourMinuteDateFormart = new SimpleDateFormat("dd/MM/yy, HH:mm");
        String formattedDate = hourMinuteDateFormart.format(date);

        return formattedDate;
    }

    public static boolean isItAlready11AM() {
        boolean rtnValue = false;
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        if(hour>=11){  ////
            rtnValue = true;
        }
        return rtnValue;
    }

    public static String getDayOfWeek(){
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        String day = "";
        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
        if(dayOfWeek ==1){
            day = "SUNDAY";
        } else if (dayOfWeek ==2){
            day = "MONDAY";
        }else if (dayOfWeek ==3){
            day = "TUESDAY";
        }else if (dayOfWeek ==4){
            day = "WEDNESDAY";
        }else if (dayOfWeek ==5){
            day = "THURSDAY";
        }else if (dayOfWeek ==6){
            day = "FRIDAY";
        }else if (dayOfWeek ==7){
            day = "SATURDAY";
        }
        return day;

    }

    public static Date getTimeYesterday() {
        DateTime today = new DateTime().withTimeAtStartOfDay();
        DateTime yesterday = today.minusDays(1).withTimeAtStartOfDay();

        return yesterday.toDate(); //covert joda time to java date
    }

    public static int getDateFieldFromFullDateToday() {
        DateTime today = new DateTime().withTimeAtStartOfDay();
        return today.getDayOfMonth();

    }

    public static boolean isItAWeekDay() {
        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        int dayOfWeek =  cal.get(Calendar.DAY_OF_WEEK);
        boolean isWeekday = ((dayOfWeek >= Calendar.MONDAY) && (dayOfWeek <= Calendar.FRIDAY));
        return isWeekday;
    }
}
