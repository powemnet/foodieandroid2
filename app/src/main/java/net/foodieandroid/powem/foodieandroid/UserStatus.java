package net.foodieandroid.powem.foodieandroid;

/**
 * Created by Power on 16/08/17. With lots of love and affection :) xoxo
 */

public enum UserStatus {
    PENDING_APPROVAL,
    ACTIVE,
    DEACTIVATED
}
