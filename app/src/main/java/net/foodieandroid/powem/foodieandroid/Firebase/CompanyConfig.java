package net.foodieandroid.powem.foodieandroid.Firebase;

import java.util.Date;

/**
 * Created by Power on 13/09/17. With lots of love and affection :) xoxo
 */

public class CompanyConfig {
    public String companyName;
    public String curreny;
    public String providerKey; //DELETE THIS LATER COZ WE'RE NOT USING IT
    public String providerName;
    public String howToPay;


    public CompanyConfig() {
    }


    public CompanyConfig(String companyName, String curreny, String status, String providerKey, String providerName, String howToPay) {
        this.companyName = companyName;
        this.curreny = curreny;
        this.providerKey = providerKey;
        this.providerName = providerName;
        this.howToPay = howToPay;
    }
}
