package net.foodieandroid.powem.foodieandroid.Login;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import net.foodieandroid.powem.foodieandroid.Posts.PostsActivity;
import net.foodieandroid.powem.foodieandroid.R;

/**
 * Created by power on 11/11/17.
 */





public class LoginActivity extends Activity implements LoginContract.View {
    private Button loginButton, registerButton;
    private EditText emailAddress;
    private EditText password;
    private TextView forgotPassword;
    private ProgressBar spinner;

    //    private SessionManager sessionManager;
    private LoginContract.ActionListener presenter;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);


        //initialise firebase
        mAuth = FirebaseAuth.getInstance();

        //intialise presenter
        presenter = new LoginPresenter(this,getApplicationContext(),mAuth);

        //initialise views
        loginButton = (Button)findViewById(R.id.btn_log_in);
        emailAddress = (EditText) findViewById(R.id.et_email_address_log_in);
        password = (EditText) findViewById(R.id.et_pin_log_in);
        spinner = (ProgressBar)findViewById(R.id.progressBar1);
        registerButton = (Button)findViewById(R.id.btn_register);
//        forgotPassword = (TextView) findViewById(R.id.tv_forgot_password);

        //set onclick listeners
        loginButton.setOnClickListener(presenter.onClickLogInButton());
        registerButton.setOnClickListener(presenter.onClickRegisterButton());
//        forgotPassword.setMovementMethod(LinkMovementMethod.getInstance());
//        forgotPassword.setOnClickListener(presenter.onForgotPassword());


    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();

        if (currentUser!=null){
            launchPostsScreen();
        }
    }


    @Override
    public void launchPostsScreen() {
        Intent intent = new Intent(this, PostsActivity.class);
        startActivity(intent);
        Toast.makeText(this, "User logged in", Toast.LENGTH_LONG).show();


    }

    @Override
    public void showUpdateDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
        alertDialog.setTitle("Update Available");
        alertDialog.setMessage("Update for a bette experience with"+ String.valueOf(R.string.app_name));
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Update",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        presenter.launchPlayStore();
                    }
                });
        alertDialog.show();
    }

    @Override
    public void showEnterEmailToast() {
        Toast.makeText(this, "First enter a valid email", Toast.LENGTH_LONG).show();
    }

    @Override
    public void showAnErrorOccuredToast() {
        Toast.makeText(this, "An error occured", Toast.LENGTH_LONG).show();

    }

    @Override
    public void showAccountDeosntExistToast() {
        Toast.makeText(this, "Account doesn't exist", Toast.LENGTH_LONG).show();

    }

    @Override
    public void showEmailSentToast() {
        Toast.makeText(this, "Email Sent!", Toast.LENGTH_LONG).show();

    }

    public String getEmailAddress() {
        return emailAddress.getText().toString();
    }

    public String getPassword() {
        return password.getText().toString();
    }

    @Override
    public void showError() {
        Toast.makeText(this, "Can't reach the VLEA Food network", Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean phoneNumberValid() {
        int length = emailAddress.getText().length();
        boolean valid = true;
        if (length<10){
            valid = false;
        }
        return valid;
    }

    @Override
    public void showPhoneError() {
        emailAddress.setError(getString(R.string.wrong_email_address));

    }

    @Override
    public boolean pinValid() {
        int length = password.getText().length();
        boolean valid = true;
        if (length<4){
            valid = false;
        }
        return valid;
    }

    @Override
    public void showPinError() {
        password.setError(getString(R.string.wrong_pin));
    }

    @Override
    public void hidePinError() {
        password.setError(null);
    }

    @Override
    public void hidePhoneError() {
        emailAddress.setError(null);
    }
    @Override
    public void showSpinner() {
        spinner.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideSpinner() {
        spinner.setVisibility(View.INVISIBLE);
    }

    @Override
    public Activity getActivity() {
        return this;
    }

}
