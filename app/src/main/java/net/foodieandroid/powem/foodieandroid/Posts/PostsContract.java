package net.foodieandroid.powem.foodieandroid.Posts;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;


import net.foodieandroid.powem.foodieandroid.Food;

import java.util.Date;

/**
 * Created by Power on 21/07/17. With lots of love and affection :) xoxo
 */

public interface PostsContract {

    interface View{

        void launchAddPostScreen();

        void launchLoginScreen();

        void showSpinner();

        void hideProgressDialog();

        void showUserNotApprovedToast();

        void showUserDeactivatedToast();

        void showItsAlready11AMToast();

        void refreshView();

        void hideSwipeRefresh();

        void showItsAWeekEndMessage();

        void showAnErrorOccuredToast();

        Context getContext();

        Date getTimeNow();

        void showYourDataIsStillUpdatingMessage();

        void showMenuScreen();
    }

    interface ActionListener{

        android.view.View.OnClickListener onClickAddPost();

        SwipeRefreshLayout.OnRefreshListener onRefreshListener();

        android.view.View.OnClickListener onClickCancelButton(Food order, String orderId);

        void onClickMenu();

        void onClickFollowers();
    }


}
