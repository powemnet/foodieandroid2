package net.foodieandroid.powem.foodieandroid;

import java.util.Date;

/**
 * Created by Power on 18/08/17. With lots of love and affection :) xoxo
 */

public class Food {

    public String userId;
    public Date orderDateTime;
    public String userDisplayName;
    public String userEmail;
    public String food;
    public String rating;
    public String venue;

    public Food() {
        // Default constructor required for calls to DataSnapshot.getValue(Food.class)
    }

    public Food(String userId, Date orderDateTime, String userDisplayName, String userEmail, String food, String rating, String venue) {
        this.userId = userId;
        this.orderDateTime = orderDateTime;
        this.userDisplayName = userDisplayName;
        this.userEmail = userEmail;
        this.food = food;
        this.rating = rating;
        this.venue = venue;
    }
}
