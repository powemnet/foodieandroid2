package net.foodieandroid.powem.foodieandroid.Posts;


import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;


import net.foodieandroid.powem.foodieandroid.Firebase.FirebaseHelper;
import net.foodieandroid.powem.foodieandroid.Food;
import net.foodieandroid.powem.foodieandroid.GenericUtil.DateUtil;
import net.foodieandroid.powem.foodieandroid.User;
import net.foodieandroid.powem.foodieandroid.UserStatus;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

/**
 * Created by Power on 21/07/17. With lots of love and affection :) xoxo
 */

public class PostsPresenter implements  PostsContract.ActionListener{

    private PostsContract.View mView;
    private String TAG = "Orders Presenter";
    private FirebaseAuth mAuth;
    private  Context mContext;
    private boolean itAWeekend;
    final String[] currentUserAmountDue = {""};



    public PostsPresenter(PostsContract.View mView, FirebaseAuth auth, Context context){
        this.mView = mView;
        this.mAuth = auth;
        this.mContext = context;
    }

    @Override
    public View.OnClickListener onClickAddPost() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddPostScreen();
            }
        };
    }

    private String showAddPostScreen() {
        mView.launchAddPostScreen();
        return "";
    }

    private String launchAddOrderScreen() {
        mView.hideProgressDialog();
        mView.launchAddPostScreen();
        return "";
    }

    @Override
    public SwipeRefreshLayout.OnRefreshListener onRefreshListener() {
        return new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mView.refreshView();
            }
        };
    }

    @Override
    public View.OnClickListener onClickCancelButton(final Food order, final String orderKey) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mView.showSpinner();
                cancelOrder(order, orderKey, new Callable<String>() {
                    @Override
                    public String call() throws Exception {
                        refreshView();
                        return checkCurrentAmountDueThenUpdateUserBalanceAndLastOrderPostedDate(order);
                    }
                });
            }
        };
    }

    @Override
    public void onClickMenu() {
            mView.showMenuScreen();

    }

    @Override
    public void onClickFollowers() {

    }


    private String refreshView() {
         mView.refreshView();
        return "";
    }

    private String cancelOrder(Food order, String orderKey, final Callable<String> command) {
        DatabaseReference mDatabase = FirebaseHelper.getFoodDbRef();

            mDatabase.child(orderKey).removeValue(new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    if(databaseError!=null){
                        mView.hideProgressDialog();
                        Toast.makeText(mView.getContext(), databaseError.getMessage(),
                                Toast.LENGTH_SHORT).show();
                    }
                    else{

                            try {
                                command.call();

                            } catch (Exception e) {
                                mView.hideProgressDialog();
                                mView.showAnErrorOccuredToast();
                                e.printStackTrace();
                            }
                    }
                }
            });

        return  "";
    }

    public String checkCurrentUserAmountDue(final Callable<String> command) {
        DatabaseReference usersRef = FirebaseHelper.getUserDbRef();

        ValueEventListener userListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                Log.d("AMOUNT DUE:------ ", user.amountDue);

                currentUserAmountDue[0] = user.amountDue;
                try {
                    command.call();
                } catch (Exception e) {
                    mView.hideProgressDialog();
                    mView.showAnErrorOccuredToast();
                    e.printStackTrace();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "addOrdersPresenter:onCancelled", databaseError.toException());
                mView.hideProgressDialog();
                Toast.makeText(mContext, databaseError.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        };
        usersRef.addListenerForSingleValueEvent(userListener);

        return currentUserAmountDue[0];
    }


    public String checkCurrentAmountDueThenUpdateUserBalanceAndLastOrderPostedDate(final Food order){

        checkCurrentUserAmountDue(new Callable<String>() {
            @Override
            public String call() throws Exception {
                return updateUserAmountDueAndLastOrderPostedDate(new Callable<String>() {
                    @Override
                    public String call() throws Exception {
                        return "";
                    }
                }, order);
            }
        });
        return "";
    }

    private String updateUserAmountDueAndLastOrderPostedDate(final Callable<String> command, Food order) {
        Integer newAmountDue = Integer.parseInt(currentUserAmountDue[0]) - Integer.parseInt("4");

        DatabaseReference mUsersDbRef = FirebaseHelper.getUserDbRef();
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/amountDue/", newAmountDue.toString());
        childUpdates.put("/latestOrderDate/", mView.getTimeNow());

        mUsersDbRef.updateChildren(childUpdates, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if(databaseError!=null){
                    mView.hideProgressDialog();
                    Toast.makeText(mContext, databaseError.getMessage(),
                            Toast.LENGTH_SHORT).show();

                    try {
                        command.call();
                    } catch (Exception e) {
                        mView.hideProgressDialog();
                        mView.showAnErrorOccuredToast();
                        e.printStackTrace();
                    }

                }
                else{
                    mView.hideProgressDialog();
                }
            }
        });

        return "justBecauseIHaveToReturnADamnString";
    }

    private String isItAlready11AM(final Callable<String> command) {
        if (DateUtil.isItAlready11AM()){
            //it's passed 12PM
            mView.hideProgressDialog();
            mView.showItsAlready11AMToast();
        }
        else{
            //it's NOT passed 11AM
            try {
                command.call();
            } catch (Exception e) {
                e.printStackTrace();
                mView.hideProgressDialog();
            }

        }
        return "";
    }


    public boolean isItAWeekday() {
        return  DateUtil.isItAWeekDay();

    }

    private String checkUserStatus(final Callable<String> command) {
        User user = FirebaseHelper.getUserObject();
        Log.d("STATUS:------ ",user.status);

        if(user.status.equals(UserStatus.ACTIVE.toString())){
            try {
                command.call();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }else if(user.status.equals(UserStatus.PENDING_APPROVAL.toString())){

            mView.hideProgressDialog();
            mView.showUserNotApprovedToast();
        }
        else if(user.status.equals(UserStatus.DEACTIVATED.toString())){

            mView.hideProgressDialog();
            mView.showUserDeactivatedToast();
        }
        return "";
    }

}
