package net.foodieandroid.powem.foodieandroid.GenericUtil;

/**
 * Created by Power on 24/08/17. With lots of love and affection :) xoxo
 */

public class IntegerUtil {

    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch(NumberFormatException e) {
            return false;
        } catch(NullPointerException e) {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }
}
