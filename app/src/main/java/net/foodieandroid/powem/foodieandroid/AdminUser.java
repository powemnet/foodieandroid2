package net.foodieandroid.powem.foodieandroid;

import java.util.Date;

/**
 * Created by Power on 13/09/17. With lots of love and affection :) xoxo
 */

public class AdminUser {
    public String username;
    public String email;
    public String status; //DELETE THIS LATER COZ WE'RE NOT USING IT
    public String userId;
    public String userKeyInCustomDb;
    public String amountDue;
    public Date lastPaymentDate;
    public Date lastOrderPostedDate;
    public String adminStatus;
    public String menu;
    public String providerName;
    public String howToPay;


    public AdminUser() {
    }


    public AdminUser(String username, String email, String status, String userId, String userKeyInCustomDb, String amountDue, Date lastPaymentDate, Date lastOrderPostedDate, String adminStatus, String menu, String providerName, String howToPay) {
        this.username = username;
        this.email = email;
        this.status = status;
        this.userId = userId;
        this.userKeyInCustomDb = userKeyInCustomDb;
        this.amountDue = amountDue;
        this.lastPaymentDate = lastPaymentDate;
        this.lastOrderPostedDate = lastOrderPostedDate;
        this.adminStatus = adminStatus;
        this.menu = menu;
        this.providerName = providerName;
        this.howToPay = howToPay;
    }
}
