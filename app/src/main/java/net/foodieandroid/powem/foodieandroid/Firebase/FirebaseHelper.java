package net.foodieandroid.powem.foodieandroid.Firebase;

import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import net.foodieandroid.powem.foodieandroid.AdminUser;
import net.foodieandroid.powem.foodieandroid.Company;
import net.foodieandroid.powem.foodieandroid.User;
import net.foodieandroid.powem.foodieandroid.UserInfoJsonKeysForDisplayNameReturnValueFromFirebase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.concurrent.Callable;

/**
 * Created by Power on 20/08/17. With lots of love and affection :) xoxo
 */

public class FirebaseHelper {
    static User mUser;
    static Company mCompany;
    static AdminUser mAdminUser;
    static String firebaseUpdatedForFirstTime = "NO";
    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private static DatabaseReference allProvidersDbref;
    private static CompanyConfig companyConfig;
    private static String TAG = "FirebaseHelper";

    public static String getUserDisplayName(){

        String userInfo = FirebaseAuth.getInstance().getCurrentUser().getDisplayName();
        JSONObject userInfoJson;
        String displayName = "";
        try {
            userInfoJson = new JSONObject(userInfo);
            displayName = userInfoJson.getString(UserInfoJsonKeysForDisplayNameReturnValueFromFirebase.DISPLAY_NAME.toString());


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return  displayName;

    }

    static class Admin{


    }

    public static String getUserKeyInCustomUserDb(){


        String userInfo = FirebaseAuth.getInstance().getCurrentUser().getDisplayName();
        JSONObject userInfoJson;
        String userKeyInCustomUserDb = "";
        try {
            userInfoJson = new JSONObject(userInfo);
            userKeyInCustomUserDb = userInfoJson.getString(UserInfoJsonKeysForDisplayNameReturnValueFromFirebase.USER_KEY_IN_CUSTOM_USER_DB.toString());


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return  userKeyInCustomUserDb;

    }

    public  static class CompanyHelper{

        public static String getCurrency() {
            return FirebaseHelper.companyConfig.curreny;
        }
    }
    public static String getUserCompanyKey(){


        String userInfo = FirebaseAuth.getInstance().getCurrentUser().getDisplayName();
        JSONObject userInfoJson;
        String companyKey = "";
        try {
            userInfoJson = new JSONObject(userInfo);
            companyKey = userInfoJson.getString(UserInfoJsonKeysForDisplayNameReturnValueFromFirebase.COMPANY.toString());


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return  companyKey;

    }



    public static String getUserEmail(){
        return  FirebaseAuth.getInstance().getCurrentUser().getEmail();
    }


    public static DatabaseReference getUserDbRef(){
        return  FirebaseDatabase.getInstance().getReference("Company/"+getUserCompanyKey()+"/users/"+getUserKeyInCustomUserDb());
    }


    public static DatabaseReference getFoodDbRef() {
        return  FirebaseDatabase.getInstance().getReference("users/"+getUserKeyInCustomUserDb()+"/food");
    }

    public static Query getQueryToSortAllPostsForThisUser(){
        Query postsQuery =  FirebaseHelper.getFoodDbRef()
                .orderByChild(DatabaseOrderKeys.userId.toString()).equalTo(FirebaseHelper.getUserId());

        return  postsQuery;
    }

    private static String getUserId() {
        return FirebaseAuth.getInstance().getCurrentUser().getUid();

    }

    public static class UserHelper{
        public static void setObject(User user){
            FirebaseHelper.mUser  = user;
        }

    }


    public static class AdminProviderUserHelper{
        public static String getHowToPayIntructions(){
            return FirebaseHelper.mAdminUser.howToPay;
        }

    }

    public static User getUserObject(){
        return FirebaseHelper.mUser;
    }

    public static DatabaseReference getAllCompaniesDbRef() {
        return  FirebaseDatabase.getInstance().getReference("Company");

    }

    public static DatabaseReference getAllProvidersDbref() {
        return  FirebaseDatabase.getInstance().getReference("adminUsers");

    }

    public static void setCompanyObject(Company company) {
        FirebaseHelper.mCompany = company;
    }


    public static String getProviderKey() {
        return FirebaseHelper.companyConfig.providerKey;
    }

    public static String getProviderName() {
        return FirebaseHelper.companyConfig.providerName;
    }

    public static void setAdminUser(AdminUser adminUser) {
        FirebaseHelper.mAdminUser = adminUser;
    }

    public static AdminUser getAdminUser() {
        return FirebaseHelper.mAdminUser;
    }

    public static String getMenuString() {
        return FirebaseHelper.mAdminUser.menu;
    }

    public static String setFirebaseUpdatedForTheFirstTime(){
        FirebaseHelper.firebaseUpdatedForFirstTime = "YES";
        return "";
    }

    public static String getFirebaseUpdatedForTheFirstTime(){
        return FirebaseHelper.firebaseUpdatedForFirstTime;
    }


    public static String getFoodForToday(String day) throws JSONException {
        String menuString = FirebaseHelper.getMenuString();
        JSONObject menuJson;
        String newDayString = "";
        String food = "";
        try {
            menuJson = new JSONObject(menuString);
            newDayString = menuJson.getString(day.toUpperCase());

            JSONObject newJson = new JSONObject(newDayString);
            food = newJson.getString(MenuItemsKeysForDisplayNameReturnValueFromFirebase.FOOD.toString());

        } catch (JSONException e) {

        }
        return food;
    }

    public static String getPriceForToday(String day) throws JSONException {
        String menuString = FirebaseHelper.getMenuString();
        JSONObject menuJson;
        String newDayString = "";
        String food = "";
        try {
            menuJson = new JSONObject(menuString);
            newDayString = menuJson.getString(day.toUpperCase());

            JSONObject newJson = new JSONObject(newDayString);
            food = newJson.getString(MenuItemsKeysForDisplayNameReturnValueFromFirebase.PRICE.toString());

        } catch (JSONException e) {

        }
        return food;
    }



    public static void setCompanyConfig(CompanyConfig companyConfig) {
        FirebaseHelper.companyConfig = companyConfig;
    }

    public static String getFoodFromMenuJson(String dayKey) throws JSONException {
        String menuString = FirebaseHelper.getMenuString();
        JSONObject menuJson;
        String dayString = "";
        String dayFood = "";
        menuJson = new JSONObject(menuString);
        dayString = menuJson.getString(dayKey);

        JSONObject dayJson;


        try {
            dayJson = new JSONObject(dayString);
            dayFood = dayJson.getString(MenuItemsKeysForDisplayNameReturnValueFromFirebase.FOOD.toString());
        }catch (JSONException e) {
            e.printStackTrace();
        }

        return dayFood;
    }

    public static String getPriceFromMenuJson(String dayKey) throws JSONException {
        String menuString = FirebaseHelper.getMenuString();
        JSONObject menuJson;
        String dayString = "";
        String dayFood = "";
        menuJson = new JSONObject(menuString);
        dayString = menuJson.getString(dayKey);

        JSONObject dayJson;


        try {
            dayJson = new JSONObject(dayString);
            dayFood = dayJson.getString(MenuItemsKeysForDisplayNameReturnValueFromFirebase.PRICE.toString());
        }catch (JSONException e) {
            e.printStackTrace();
        }

        return dayFood;
    }

    public static String getUserAmountDue() {
        return mUser.amountDue;
    }

    public static Date getLastPaymentDate() {
        return mUser.lastPaymentDate;
    }

    public static String getCompanyHowToPay() {
        return FirebaseHelper.companyConfig.howToPay;
    }


    public static String updateCompanyConfig(final Callable<String> command) {
        DatabaseReference companyRef = FirebaseDatabase.getInstance().getReference("Company/"+ FirebaseHelper.getUserCompanyKey()).child("/config");
        ValueEventListener companyListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                CompanyConfig companyConfig = dataSnapshot.getValue(CompanyConfig.class);
                FirebaseHelper.setCompanyConfig(companyConfig);
                Log.d("UPDATED COMPANY CONFIG", "--------------STARTED");

                try {
                    command.call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }



            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, ":onCancelled", databaseError.toException());
                Log.d(TAG,  "---------------"+databaseError.toString());
            }
        };
        companyRef.addValueEventListener(companyListener);
        return "";
    }


    public static String updateAdminuserObject(final Callable<String> command) {
        DatabaseReference adminUserRef = FirebaseDatabase.getInstance().getReference("adminUsers/"+FirebaseHelper.getProviderKey());
        ValueEventListener adminUserListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                AdminUser adminUser = dataSnapshot.getValue(AdminUser.class);
                FirebaseHelper.setAdminUser(adminUser);
                Log.d("UPDATED ADMIN OBJECT", "--------------STARTED");

                try {
                    command.call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
//                Log.w(TAG, ":onCancelled", databaseError.toException());
            }
        };
        adminUserRef.addValueEventListener(adminUserListener);
        return "";
    }


    public static String updateUserObject(final Callable<String> command) {
        DatabaseReference usersRef = FirebaseHelper.getUserDbRef();
        ValueEventListener userListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                UserHelper.setObject(user);
                Log.d("UPDATE USER OBJECT:-- ","--------SUCCESS"+user.status);
                try {
                    command.call();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "addOrdersPresenter:onCancelled", databaseError.toException());
                Log.d("UPDATE USER OBJECT:-- ","--------FAILED");

                //TODO ADD A BROADCAST IN HERE, AND IN EACH VIEW, SHOW THE TOAST "AN ERROR OCCURED", WHEN THIS BROADCAST IS CAST. DO FOR ALL METHODS WITH AN ONCANCELLED CALLBACK - 22 Sept

            }
        };
        usersRef.addListenerForSingleValueEvent(userListener);

        return "";
    }

//    public static String updateAllTheDataBruuhhhhhh(final Callable<String> command) {
//        updateCompanyConfig(new Callable<String>() {
//            @Override
//            public String call() throws Exception {
//                return updateAdminuserObject(new Callable<String>() {
//                    @Override
//                    public String call() throws Exception {
//                        return updateUserObject(new Callable<String>() {
//                            @Override
//                            public String call() throws Exception {
//                                return command.call();
//                            }
//                        });
//                    }
//                });
//            }
//        });
//        return "";
//    }


}
