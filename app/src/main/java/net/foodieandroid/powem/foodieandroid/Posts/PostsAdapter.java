package net.foodieandroid.powem.foodieandroid.Posts;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;


import net.foodieandroid.powem.foodieandroid.Firebase.FirebaseHelper;
import net.foodieandroid.powem.foodieandroid.Food;
import net.foodieandroid.powem.foodieandroid.GenericUtil.DateUtil;
import net.foodieandroid.powem.foodieandroid.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Power on 02/03/17. With lots of love and affection :) xoxo
 */


public  class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.ViewHolder> {

    private Context mContext;
    private ChildEventListener mChildEventListener;
    private FirebaseAuth mAuth;
    private List<String> mFoodIds = new ArrayList<>();
    private List<Food> mFood = new ArrayList<>();

    private static final String TAG = "Orders Adapter";

    private PostsContract.View mView;
    private PostsContract.ActionListener mPresenter;
    private Query userPostsQuery;



    public PostsAdapter(final Context context, final FirebaseAuth auth, PostsContract.View view, PostsContract.ActionListener presenter) {
        mContext = context;
        mAuth = auth;
        mView = view;
        mPresenter = presenter;
//        userPostsQuery = FirebaseHelper.getQueryToSortAllPostsForThisUser();
        userPostsQuery = FirebaseHelper.getFoodDbRef();

        // Create child event listener
        // [START child_event_listener_recycler]
        ChildEventListener childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                Log.d(TAG, "onChildAdded:" + dataSnapshot.getKey());

                // A new order has been added, add it to the displayed list
                Food order = dataSnapshot.getValue(Food.class);
//                if(order.userId.equals(auth.getCurrentUser().getUid())){

                    mView.showSpinner();
                    // Update RecyclerView
                    mFoodIds.add(dataSnapshot.getKey());
                    mFood.add(order);
//                    notifyItemInserted(mFood.size() - 1);
                    notifyDataSetChanged();
//                }
                mView.hideProgressDialog();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {
                mView.hideProgressDialog();

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                mView.hideProgressDialog();

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {
                mView.hideProgressDialog();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "orders:onCancelled", databaseError.toException());
                Toast.makeText(mContext, "Failed to load orders.",
                        Toast.LENGTH_SHORT).show();
                mView.hideProgressDialog();
            }
        };
        userPostsQuery.addChildEventListener(childEventListener);
        // Store reference to listener so it can be removed on app stop
        mChildEventListener = childEventListener;
    }


    public static   class ViewHolder extends RecyclerView.ViewHolder {
        private TextView food, venue, rating;
        private ImageView cancelButton;

        public ViewHolder(View itemView) {
            super(itemView);
            food = (TextView) itemView.findViewById(R.id.tv_food);
            venue = (TextView) itemView.findViewById(R.id.tv_venue);
            rating = (TextView) itemView.findViewById(R.id.tv_rating);
            cancelButton = (ImageView)itemView.findViewById(R.id.image_cancel_order);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.post_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Food food = mFood.get(position);
        holder.food.setText(food.food);
        holder.venue.setText(food.venue);
        holder.rating.setText(food.rating);
    }

    @Override
    public int getItemCount() {
        return mFood.size();
    }

    public void cleanupListener() {
        if (mChildEventListener != null) {
            userPostsQuery.removeEventListener(mChildEventListener);
        }
    }
//
//    public void refresh() {
//
//        userPostsQuery.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//
//                mFood.clear();
//                for (DataSnapshot child: dataSnapshot.getChildren()) {
//                    mFood.add(child.getValue(Food.class));
//                }
//                notifyDataSetChanged();
//                mView.hideSwipeRefresh();
//                mView.hideProgressDialog();
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//                // Getting Post failed, log a message
//                Log.w(TAG, "loadUsers:onCancelled", databaseError.toException());
//                mView.hideSwipeRefresh();
//                mView.hideProgressDialog();
//            }
//        });
//
//    }

}