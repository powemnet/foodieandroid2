package net.foodieandroid.powem.foodieandroid.Register;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import net.foodieandroid.powem.foodieandroid.BaseActivity;
import net.foodieandroid.powem.foodieandroid.R;


/**
 * Created by Power on 15/08/17. With lots of love and affection :) xoxo
 */

public class RegisterActivity extends BaseActivity {



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        RegisterFragment fragment = new RegisterFragment();
        fragmentTransaction.add(R.id.content_frame_base_activity,fragment);
        fragmentTransaction.commit();

    }

}
