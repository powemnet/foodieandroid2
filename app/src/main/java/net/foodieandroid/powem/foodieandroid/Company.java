package net.foodieandroid.powem.foodieandroid;

import java.util.Date;

/**
 * Created by Power on 16/08/17. With lots of love and affection :) xoxo
 */

public class Company {

    public String companyName;

    public Company() {
    }

    public Company(String companyName) {

        this.companyName = companyName;
    }

}
