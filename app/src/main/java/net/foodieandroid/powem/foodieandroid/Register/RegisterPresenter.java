package net.foodieandroid.powem.foodieandroid.Register;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import net.foodieandroid.powem.foodieandroid.Firebase.FirebaseHelper;
import net.foodieandroid.powem.foodieandroid.GenericUtil.DateUtil;
import net.foodieandroid.powem.foodieandroid.User;
import net.foodieandroid.powem.foodieandroid.UserInfoJsonKeysForDisplayNameReturnValueFromFirebase;
import net.foodieandroid.powem.foodieandroid.UserStatus;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by Power on 18/02/17. With lots of love and affection :) xoxo
 */

public class RegisterPresenter implements RegisterContract.ActionListener {
    private final RegisterContract.View mView;
    private final  FirebaseAuth mAuth;
    private final Context context;
    private List<String> mCompanyIds = new ArrayList<>();
    private List<String> mCompanies = new ArrayList<>();

    private static final String TAG = "Register User";

    public RegisterPresenter(RegisterContract.View view, FirebaseAuth auth, Context context){
        this.mView = view;
        this.mAuth = auth;
        this.context = context;
    }

    @Override
    public View.OnClickListener onClickLogInButton() {


        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (phoneNumberValid()&&pinValid()){
//                    Log.d("Login Presenter---","in if");
                    mView.showProgressDailog();
                    sendLoginRequest();


                }
                else{
                    if(!phoneNumberValid()){
                        mView.showPhoneError();
                    }
                    if(!pinValid()){
                        mView.showPinError();
                    }
                }

            }
        };
    }

    @Override
    public TextWatcher onPhoneNumberTextChanged() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mView.hidePhoneError();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
    }

    @Override
    public TextWatcher onPinTextChanged() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mView.hidePinError();
                mView.getEmailAddress();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
    }

    @Override
    public View.OnClickListener onClickRegisterButton() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View btnView) {

                if(fieldsAreNotEmpty()){
                    String username = mView.getUserName();
                    String email = mView.getEmailAddress();
                    String password =  mView.getPassword();
                    mView.showProgressDailog();
                    registerNewUser_updateCompanyConfig_addUserToUsersCustomDb(username, email, password);
                }
                else {
                    Toast.makeText(context, "Please fill in all fields",
                            Toast.LENGTH_SHORT).show();
                }

            }


        };
    }

    private boolean fieldsAreNotEmpty() {
        boolean value = false;
        if(!(mView.getPassword().isEmpty()) && !(mView.getUserName().isEmpty()) && !(mView.getEmailAddress().isEmpty())){
            value = true;
        }
        else{
            value = false;
        }
        return value;
    }

    @Override
    public TextWatcher onUserNameTextChanged() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mView.hideuserNameError();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
    }

    @Override
    public void getSpinnerValues() {

        getCompanies(new Callable<String>() {
            @Override
            public String call() throws Exception {
                return updateSpinnervaluesInView();
            }
        });

    }

    private String updateSpinnervaluesInView() {
        mView.updatedSpinnerValues(mCompanies);

        return "";
    }


    private String getCompanies(final Callable<String> command) {
        DatabaseReference companiesDbRef = FirebaseHelper.getAllCompaniesDbRef();

        companiesDbRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                mCompanies.clear();
                for (DataSnapshot child: dataSnapshot.getChildren()) {
                    mCompanies.add(child.getKey());

                }
                try {
                    command.call();
                } catch (Exception e) {
                    e.printStackTrace();
                    mView.showAnErrorOccuredError();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w(TAG, "loadUsers:onCancelled", databaseError.toException());
                mView.showAnErrorOccuredError();

            }
        });
//        mView.detachEventListener();
        return "";
    }


    private String registerNewUser(final String userName, final String email, final String password, final Callable<String> command) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(mView.getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "------------------------createUserWithEmail:success");
                            try {
                                command.call();
                                Log.d(TAG, "------------------------createUserWithEmail:success");

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(context, task.getException().getMessage(),
                                    Toast.LENGTH_LONG).show();
                            mView.hideProgressBar();

                        }

                    }
                });
        return "";

    }

    private void registerNewUser_updateCompanyConfig_addUserToUsersCustomDb(final String username, final String email, final String password) {
        registerNewUser(username, email, password, new Callable<String>() {
            @Override
            public String call() throws Exception {
                return addUserToUsersCustomDb(username, email);
            }
        });
    }


    private String addUserToUsersCustomDb(String userName, String email) {

        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("users");
        final String keyInCustomDb = mDatabase.push().getKey();

        User user = new User(mAuth.getCurrentUser().getUid(), userName,email, UserStatus.PENDING_APPROVAL.toString(),keyInCustomDb,"0", mView.getTimeNow(), DateUtil.getTimeYesterday());

        // push user to 'users' node using the tripId
        mDatabase.child(keyInCustomDb).setValue(user, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if(databaseError!=null){

                    mView.hideProgressBar();
                    Toast.makeText(context, databaseError.getMessage(),
                            Toast.LENGTH_SHORT).show();
                }
                else{
                    updateFirebaseUserProfileNameWithTheirIdInCustomDbInfoAndCompanyNameAndProviderForReference(keyInCustomDb);
                    mView.hideProgressBar();


                }
            }
        });

return "";
    }


    private void updateFirebaseUserProfileNameWithTheirIdInCustomDbInfoAndCompanyNameAndProviderForReference(String keyInCustomDb) {
        String userInfoString = "";
        try {
            userInfoString = new JSONObject()
                    .put(UserInfoJsonKeysForDisplayNameReturnValueFromFirebase.DISPLAY_NAME.toString(), mView.getUserName())
                    .put(UserInfoJsonKeysForDisplayNameReturnValueFromFirebase.USER_KEY_IN_CUSTOM_USER_DB.toString(), keyInCustomDb)
                    .put(UserInfoJsonKeysForDisplayNameReturnValueFromFirebase.COMPANY.toString(), "company").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(userInfoString)
                .build();

        user.updateProfile(profileUpdates)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "User profile updated.");
                            mView.hideProgressBar();
                            mView.launchPostsScreen();
                        }
                        else {
                            Log.w(TAG, "updateUserProfile:failure", task.getException());
                            Toast.makeText(context, task.getException().getMessage(),
                                    Toast.LENGTH_LONG).show();
                            mView.hideProgressBar();

                        }
                    }
                });
    }

    private boolean pinValid() {
        return mView.pinValid();
    }

    private boolean phoneNumberValid() {
        return mView.phoneNumberValid();
    }

    private void sendLoginRequest() {


    }



}
