package net.foodieandroid.powem.foodieandroid;

/**
 * Created by Power on 16/08/17. With lots of love and affection :) xoxo
 */

public class ProviderKey {

    public String providerKey;

    public ProviderKey() {
    }

    public ProviderKey(String providerKey) {

        this.providerKey = providerKey;
    }

}
