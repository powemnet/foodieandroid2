package net.foodieandroid.powem.foodieandroid.Firebase;

/**
 * Created by Power on 20/08/17. With lots of love and affection :) xoxo
 */

public enum MenuItemsKeysForDisplayNameReturnValueFromFirebase {

    FOOD,
    PRICE
}
