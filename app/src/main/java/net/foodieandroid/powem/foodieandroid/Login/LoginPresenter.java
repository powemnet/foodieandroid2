package net.foodieandroid.powem.foodieandroid.Login;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;


import net.foodieandroid.powem.foodieandroid.Firebase.DatabaseUsersKeys;
import net.foodieandroid.powem.foodieandroid.Firebase.FirebaseHelper;
import net.foodieandroid.powem.foodieandroid.Register.RegisterActivity;
import net.foodieandroid.powem.foodieandroid.User;

import java.util.concurrent.Callable;

/**
 * Created by power on 11/11/17.
 */



public class LoginPresenter implements LoginContract.ActionListener {
    private final LoginContract.View mView;
    private Context mContext;
    private FirebaseAuth mAuth;
    private String TAG = "Login Presenter";


    public LoginPresenter(LoginContract.View view, Context context, FirebaseAuth auth){
        this.mView = view;
        this.mContext = context;
        this.mAuth = auth;
    }

    @Override
    public View.OnClickListener onClickLogInButton() {


        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(fieldsAreNotEmpty()){
                    mView.showSpinner();
                    loginUser(mView.getEmailAddress(), mView.getPassword());
                }
                else {
                    Toast.makeText(mContext, "Please fill in all fields",
                            Toast.LENGTH_SHORT).show();
                }

            }
        };
    }

    private boolean fieldsAreNotEmpty() {
        boolean value = false;
        if(!(mView.getPassword().isEmpty()) && !(mView.getEmailAddress().isEmpty())){
            value = true;
        }
        else{
            value = false;
        }
        return value;
    }



    @Override
    public View.OnClickListener onClickRegisterButton() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, RegisterActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
        };
    }


    @Override
    public void launchPlayStore() {

    }

    @Override
    public View.OnClickListener onForgotPassword() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(emailAddressIsNotEmpty()){

                    checkIfThisUserExists(new Callable<String>() {
                        @Override
                        public String call() throws Exception {
                            return sendResetPasswordEmail();
                        }
                    });

                }
                else {
                    mView.hideSpinner();
                    mView.showEnterEmailToast();
                }
            }


        };
    }

    private boolean emailAddressIsNotEmpty() {
        boolean rtnValue = false;
        if(mView.getEmailAddress().length()>0){
            rtnValue = true;
        }
        return rtnValue;
    }

    private String checkIfThisUserExists(final Callable<String> command){

        Query usersQuery = FirebaseHelper.getUserDbRef()
                .orderByChild(DatabaseUsersKeys.email.toString()).equalTo(mView.getEmailAddress());

        ValueEventListener userListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user =  new User();

                for (DataSnapshot child: dataSnapshot.getChildren()) {
                    user = child.getValue(User.class);
                }

                if(user!=null){
                    try {
                        command.call();
                    } catch (Exception e) {
                        mView.hideSpinner();
                        mView.showAnErrorOccuredToast();
                        e.printStackTrace();
                    }

                }else {
                    mView.hideSpinner();
                    mView.showAccountDeosntExistToast();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "checkUserStatusBeforeLoginPresenter:onCancelled", databaseError.toException());
                mView.hideSpinner();
                Toast.makeText(mContext, databaseError.getMessage(),
                        Toast.LENGTH_SHORT).show();

            }
        };
        usersQuery.addListenerForSingleValueEvent(userListener);

        return "";
    }

    private String sendResetPasswordEmail() {

        mView.showSpinner();
        FirebaseAuth auth = FirebaseAuth.getInstance();
        String emailAddress = mView.getEmailAddress();

        auth.sendPasswordResetEmail(emailAddress)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "Email sent.");
                            mView.hideSpinner();
                            mView.showEmailSentToast();
                        }else {
                            mView.hideSpinner();
                            mView.showAnErrorOccuredToast();

                        }
                    }
                });
        return "";
    }


    private boolean pinValid() {
        return mView.pinValid();
    }

    private boolean phoneNumberValid() {
        return mView.phoneNumberValid();
    }

    private void loginUser(final String email, final String password) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(mView.getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithEmail:success");
                            mView.hideSpinner();
                            mView.launchPostsScreen();

                        } else {
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            mView.hideSpinner();
                            Toast.makeText(mContext, task.getException().getMessage(),
                                    Toast.LENGTH_SHORT).show();

                        }
                    }
                });

    }

}
