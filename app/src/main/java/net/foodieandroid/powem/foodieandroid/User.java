package net.foodieandroid.powem.foodieandroid;

import java.util.Date;

/**
 * Created by Power on 16/08/17. With lots of love and affection :) xoxo
 */

public class User {

    public String userId;
    public String username;
    public String email;
    public String status;
    public String userKeyInCustomDb;
    public String amountDue;
    public Date lastPaymentDate;
    public Date lastOrderPostedDate;
    public String company;
    public String currency;

    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public User(String userId, String username, String email, String status, String userKeyInCustomDb, String amountDue, Date lastPaymentDate,Date lastOrderPostedDate) {

        this.userId = userId;
        this.username = username;
        this.email = email;
        this.status = status;
        this.userKeyInCustomDb = userKeyInCustomDb;
        this.amountDue = amountDue;
        this.lastPaymentDate = lastPaymentDate;
        this.lastOrderPostedDate = lastOrderPostedDate;
    }

    public String getUserId(){
        return this.userId;
    }

    public void setUserId(String userId){
        this.userId = userId;
    }

    public Date getLatestOrderDate(){
        return this.lastOrderPostedDate;
    }

    public void setLatestOrderDate(Date latestOrderDate){
        this.lastOrderPostedDate = latestOrderDate;
    }
}
