package net.foodieandroid.powem.foodieandroid.Register;

import android.app.Activity;
import android.text.TextWatcher;

import java.util.Date;
import java.util.List;

/**
 * Created by Power on 15/08/17. With lots of love and affection :) xoxo
 */

public interface RegisterContract {

    interface View{

        String getEmailAddress();

        String getPassword();

        void showError();

        boolean phoneNumberValid();

        void showPhoneError();

        boolean pinValid();

        void showPinError();

        void hidePinError();

        void hidePhoneError();

        void showProgressDailog();

        void hideProgressBar();

        Activity getActivity();

        String getUserName();

        void hideuserNameError();

        void launchPostsScreen();

        void showFailureError();

        int getPasswordLength();

        Date getTimeNow();

        String getCompanySpinnerText();

        void updatedSpinnerValues(List<String> mCompanies);

        void showAnErrorOccuredError();
    }

    interface ActionListener{


        android.view.View.OnClickListener onClickLogInButton();

        TextWatcher onPhoneNumberTextChanged();

        TextWatcher onPinTextChanged();

        android.view.View.OnClickListener onClickRegisterButton();

        TextWatcher onUserNameTextChanged();

        void getSpinnerValues();
    }
}
