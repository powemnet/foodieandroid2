package net.foodieandroid.powem.foodieandroid;

/**
 * Created by Power on 20/08/17. With lots of love and affection :) xoxo
 */

public enum UserInfoJsonKeysForDisplayNameReturnValueFromFirebase {

    USER_KEY_IN_CUSTOM_USER_DB,
    DISPLAY_NAME,
    COMPANY,

}
