package net.foodieandroid.powem.foodieandroid.AddPost;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;


import net.foodieandroid.powem.foodieandroid.Posts.PostsActivity;
import net.foodieandroid.powem.foodieandroid.R;
import net.foodieandroid.powem.foodieandroid.SnackbarDismissListener;

import java.util.Date;

/**
 * Created by Power on 18/08/17. With lots of love and affection :) xoxo
 */

public class AddPostFragment extends Fragment implements  AddPostContract.View{
    private AddPostContract.ActionListener presenter;
    private Button addFood;
    private EditText whatDidYouEat, whereDidYouEat,ratingOutOf10;

    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private  View root;
//    private


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.add_post_fragment, container, false);

        mAuth = FirebaseAuth.getInstance();

        //initialise presenter
        presenter = new AddPostPresenter(this,getContext());

        //initialise views
        addFood = (Button)root.findViewById(R.id.btn_post_food);
       whatDidYouEat = (EditText)root.findViewById(R.id.what_did_you_eat);
        whereDidYouEat = (EditText)root.findViewById(R.id.where_did_you_eat);
        ratingOutOf10 = (EditText)root.findViewById(R.id.rating_out_of_10);

        //set onclick listeners
        addFood.setOnClickListener(presenter.onClickAddFoodButton());

        //init views
//        presenter.initView();

        return root;
    }

    @Override
    public Date getTimeNow() {
        return new Date();
    }

    @Override
    public String getUserId() {
        return mAuth.getCurrentUser().getUid();
    }

    @Override
    public void hideProgressDialog() {
//        spinner.setVisibility(View.INVISIBLE);

    }

    @Override
    public void launchPostsScreen() {
        Intent intent = new Intent(getActivity(), PostsActivity.class);
        startActivity(intent);
    }

    @Override
    public void showProgressDialog() {
//        spinner.setVisibility(View.VISIBLE);

    }

    @Override
    public void showAnErrorOccuredToast() {
        Toast.makeText(getActivity().getApplicationContext(),"An unexpected error occured", Toast.LENGTH_LONG).show();
    }

    @Override
    public void showOrderAlreadyPlacedTodayMessage() {
        Snackbar snackbar = Snackbar.make(root.findViewById(R.id.add_order_coordinator_layout),
                "You already placed an order today. You can't be that hungry bruh!", Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(R.string.dismiss_string, new SnackbarDismissListener());
        snackbar.show();
    }

    @Override
    public int getNumberOfOrders() {
        int total = 0;
//        if(numberOfOrders.length()>0){
//            total = Integer.valueOf(numberOfOrders.getText().toString());
//        }
        return total;
    }

    @Override
    public void showEnterOrdersToast() {
        Toast.makeText(getActivity().getApplicationContext(),"You have not entered any orders bruh!", Toast.LENGTH_LONG).show();
    }

    @Override
    public void setDayText(String dayOfWeek) {
//        day.setText(dayOfWeek);
    }

    @Override
    public void setFoodText(String foodForToday) {
//        food.setText(foodForToday);
    }

    @Override
    public void setPriceText(String priceForToday) {
//        price.setText(priceForToday);
    }

    @Override
    public String getFoodText() {
        return whatDidYouEat.getText().toString();

    }

    @Override
    public String getPriceText() {
//        return price.getText().toString();
        return "";
    }

    @Override
    public String getRatingText() {
        return ratingOutOf10.getText().toString();
    }

    @Override
    public String getVenueText() {
        return whereDidYouEat.getText().toString();
    }

    @Override
    public void disableAddFoodButton() {
        addFood.setEnabled(false);
    }

}
